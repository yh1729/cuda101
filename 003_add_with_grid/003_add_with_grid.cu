#include <cuda_runtime.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

__global__ void add_with_grid(int n, float *x, float *y) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < n; i += stride) {
    y[i] += x[i];
  }
}

int main(void) {
  int N = 1 << 20;
  float *x;
  float *y;
  cudaMallocManaged(&x, N * sizeof(float));
  cudaMallocManaged(&y, N * sizeof(float));
  for (int i = 0; i < N; ++i) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }
  int block_size = 256;
  int num_blocks = (N + block_size - 1) / block_size;
  add_with_grid<<<num_blocks, block_size>>>(N, x, y);
  cudaDeviceSynchronize();
  float max_error = 0.0f;
  for (int i = 0; i < N; ++i) {
    max_error = fmax(max_error, fabs(y[i] - 3.0f));
  }
  printf("max error: %f\n", max_error);
  cudaFree(y);
  cudaFree(x);
  return 0;
}
