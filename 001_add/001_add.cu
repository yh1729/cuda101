#include <cuda_runtime.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

__global__ void add(int n, float *x, float *y) {
  for (int i = 0; i < n; ++i) {
    y[i] += x[i];
  }
}

int main(void) {
  int N = 1 << 20;
  float *x;
  float *y;
  cudaMallocManaged(&x, N * sizeof(float));
  cudaMallocManaged(&y, N * sizeof(float));
  for (int i = 0; i < N; ++i) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }
  add<<<1, 1>>>(N, x, y);
  cudaDeviceSynchronize();
  float max_error = 0.0f;
  for (int i = 0; i < N; ++i) {
    max_error = fmax(max_error, fabs(y[i] - 3.0f));
  }
  printf("max error: %f\n", max_error);
  cudaFree(y);
  cudaFree(x);
  return 0;
}
